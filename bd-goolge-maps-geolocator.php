<?php
/*
Plugin Name: BD Google Maps Geolocator
Author:      Ben DeGiglio
*/ 
$siteURL = dirname(__FILE__);
$db_link = $wpdb->prefix.'bd_google_maps_link';
$db_table = $wpdb->prefix.'bd_google_maps';
$db_so = $wpdb->prefix.'bd_google_maps_so';
$blog_id = get_current_blog_id();
function bd_load_admin_files(){
    wp_enqueue_script( 'custom_js', plugins_url( '/assets/js/bd_maps.js', __FILE__ ), array('jquery') );
    wp_enqueue_style( 'custom_css',plugins_url('/assets/css/bd_maps.css', __FILE__ ) );
    wp_enqueue_script( 'color_picker_js', plugins_url( '/assets/color-picker/spectrum.js', __FILE__ ), array('jquery') );
    wp_enqueue_style( 'color_picker_css',plugins_url('/assets/color-picker/spectrum.css', __FILE__ ) );
    wp_enqueue_script('media-upload');
    wp_enqueue_script('thickbox');
    wp_enqueue_style('thickbox');
}
add_action('admin_enqueue_scripts', 'bd_load_admin_files');
// [bd_locations json="false" amount="6" services_offered="true"]
function bd_load_frontend_files() {
	wp_register_script( 'bd_ajax_handle', plugins_url( '/assets/js/bd-ajax.js', __FILE__ ), array('jquery') );
	wp_localize_script( 'bd_ajax_handle', 'bd_maps_object', array( 'ajax_url' => admin_url( 'admin-ajax.php') ) );
	wp_enqueue_script( 'bd_ajax_handle' );
    wp_enqueue_style( 'style-name',  plugins_url( '/assets/css/bd_maps.css', __FILE__ ));
    // wp_enqueue_script( 'custom_js', plugins_url( '/assets/js/bd_maps.js', __FILE__ ), array('jquery') );
    $maps_options = get_option('bd_maps_api_options');
    if( isset($maps_options['users_location']) ){
		wp_enqueue_script( 'location_js', plugins_url( '/assets/js/bd_user_location.js', __FILE__ ), array('jquery') );
	}

}
add_action( 'wp_enqueue_scripts', 'bd_load_frontend_files' );
// add the admin options page
add_action('admin_menu', 'plugin_admin_add_page');
function plugin_admin_add_page() {
	add_menu_page('BD Locations Plugin Page', 'Locations', 'manage_options', 'bd-locations', 'bd_plugin_options_page', 'dashicons-location-alt');
}

function bd_plugin_options_page() { 
	global $blog_id;
	global $db_link;
	global $db_table;
	global $db_so;
	$display_options = get_option('bd_maps_display_fields');	
	$doso = isset($display_options['services_offered']) ? $display_options['services_offered'] : '';
	?>

	<div>
		<div id="settingsMsg" class="updated settings-message">
			<p><strong><?php _e('Saved Successfully!') ?></strong></p>
		</div>
		<?php if( isset($_REQUEST['err']) ) { ?>
		<div id="errorMsg" class="error">
			<p><strong><?php _e('Well, this is embarrassing. We couldnt find that location. Help us out and use the map below this time.') ?></strong></p>
		</div>
		<?php } ?>
		<?php if( isset($_GET['settings-updated']) ) { ?>
			<script type="text/javascript">
				window.onload = function(){
					document.getElementById('settingsMsg').style.display = "block";
				}	
			</script>
		<?php } ?>
		<h2>Google Maps Geolocator</h2>
		<ul class="bd-tabs">
			<li class="bd-tab <?php if ($_REQUEST['loc'] == 'false' || $_REQUEST['loc'] == false){echo 'active';} ?>" data-id="bd-tab-1">Settings</li>
			<li class="bd-tab <?php if ($_REQUEST['loc'] == 'true' || $_REQUEST['action'] == 'edit'){echo 'active';} ?>" data-id="bd-tab-2">Locations</li>
		</ul>
		<?php 
		if (isset($_REQUEST['loc'])) {
			if ($_REQUEST['loc'] == 'false') {
				$locations = false;
			}
		}
		?>
		<div class="bd-tab-container">
			<div class="bd-tab-1 bd-tab-content" <?php if ($_REQUEST['loc'] == 'false' || $_REQUEST['loc'] == false){echo 'style="display:block;"';} ?>>
				<h2>General Settings</h2>
				<form action="options.php" method="post">
				<?php settings_fields('bd_maps_plugin_options'); ?>
				<?php do_settings_sections('bd_maps_plugin'); ?>
				<?php submit_button('Save General Settings'); ?>
				</form>
				<h2>Fields Settings</h2>
				<form action="options.php" method="post">
				<?php settings_fields('bd_maps_display_settings'); ?>
				<?php do_settings_sections('bd_maps_d'); ?>
				<?php submit_button('Save Fields Settings'); ?>
				</form>
				<?php if (isset($display_options['services_offered'])): ?>
				<h2>Service Offered</h2>
				<form id="bdAddSo" action="<?php echo plugin_dir_url(__FILE__).'bdServicesOffered.php'; ?>" method="post">
					<table class="form-table">
						<tr>
							<th scope="row"></th>
							<td>
								<div class="bd-so-container">
								<?php
								global $wpdb;
								$services_offered = $wpdb->get_results("SELECT * FROM $db_so", ARRAY_A);
								foreach ($services_offered as $so) {
									$sever_off = $so['services_offered']; 
									$soID = $so['id']; 
									?>
									<div class="so-input-wrap"><input disabled class="bd-so-input" size="80" name="services_offered" type="text" value="<?php echo $sever_off; ?>"><a class="bd-edit-so-row" href="#edit"><span class="dashicons dashicons-edit"></span></a><a class="bd-remove-so-row" href="/wp-content/plugins/bd-google-maps/deleteSo.php?id=<?php echo $soID; ?>"><span class="dashicons dashicons-no"></span></a><a data-id="<?php echo $soID; ?>" class="bd-save-so-row" href="#save"><span class="dashicons dashicons-yes"></span></a></div>
								<?php } ?>
								</div>
								<a class="bd-add-so" href="#add">+ Add</a>
							</td>
						</tr>
					</table>
					<p class="bd-so-submit"><input value="Save Service Offered" class="button button-primary" type="submit"></p>
				</form>
				<?php endif; ?>
				<h2>Display Settings</h2>
				<form action="options.php" method="post">
				<?php settings_fields('bd_map_settings_1'); ?>
				<?php do_settings_sections('bd_maps'); ?>
				<?php submit_button('Save Display Settings'); ?>
				</form>
			</div>
			<div class="bd-tab-2 bd-tab-content" <?php if ($_REQUEST['loc'] == 'true' || $_REQUEST['action'] == 'edit'){echo 'style="display:block;"';} ?>>
				<?php 
				global $wpdb;
				global $db_link;
				global $db_table;
				global $db_so;
				if ($_REQUEST['id']) {
					$locationID = $_REQUEST['id'];
					$action = plugin_dir_url(__FILE__).'bdEditData.php?id='.$locationID;
					$editLocation = $wpdb->get_row( "SELECT * FROM $db_table WHERE id = $locationID", ARRAY_A);
					$buttonLabel = 'Update ';
				}else{
					$locationID = '';
					$action = plugin_dir_url(__FILE__).'bdAddData.php';
					$buttonLabel = 'Add ';
				}
				$badChars = array('<br>', '\n');
				$store_name = isset($_REQUEST['id']) ? $editLocation['store_name'] : '';
				$store_number =  isset($_REQUEST['id']) ? $editLocation['store_number'] : '';
				$state =  isset($_REQUEST['id']) ? $editLocation['state'] : '';
				$country =  isset($_REQUEST['id']) ? $editLocation['country'] : '';
				$contact_info = isset($_REQUEST['id']) ? str_replace($badChars,"\n",$editLocation['contact_info']) : '';
				$address =  isset($_REQUEST['id']) ?  str_replace($badChars,"\n",$editLocation['address']) : '';
				$store_hours =  isset($_REQUEST['id']) ?  str_replace($badChars,"\n",$editLocation['store_hours']) : '';
				$dealer_website = isset($_REQUEST['id']) ? $editLocation['dealer_website']  : '';
				$additional_information =  isset($_REQUEST['id']) ? str_replace($badChars,"\n",$editLocation['additional_information'])  : '';
				$email =  isset($_REQUEST['id']) ? $editLocation['email'] : '';
				$image =  isset($_REQUEST['id']) ? $editLocation['image'] : '';
				$latitude =  isset($_REQUEST['id']) ? $editLocation['lat'] : '';
				$longitude =  isset($_REQUEST['id']) ? $editLocation['lng'] : '';
				?>
				<form id="bdAddData" action="<?php echo $action; ?>" method="post">
					<table class="form-table">
						<?php if (isset($display_options['store_name'])): ?>
						<tr>
							<th scope="row">Store Name</th>
							<td>
								<input size='80' class="store_name" name="store_name" type="text" value="<?php echo $store_name; ?>">
							</td>
						</tr>
						<?php endif ?>
						<?php if (isset($display_options['store_number'])): ?>
						<tr>
							<th scope="row">Store Number</th>
							<td>
								<input size='80' class="store_number" name="store_number" type="text" value="<?php echo $store_number; ?>">
							</td>
						</tr>
						<?php endif ?>
						<?php if (isset($display_options['address'])): ?>
						<tr>
							<th scope="row">Address</th>
							<td>
								<textarea class="address" name="address" cols="78" rows="4"><?php echo $address; ?></textarea>
							</td>
						</tr>
						<tr id="bdAdminCoords" <?php if ( !isset($_REQUEST['err']) && !isset($_REQUEST['id']) ){echo 'class="bdhidden"';} ?>>
							<th scope="row">Coordinates</th>
							<td>
								<span style="width: 293px;float:left;">
									<label style="display: block;" for="lat">Latitude</label>
									<input id="latbox" size='38' class="lat" name="lat" type="text" value="<?php echo $latitude; ?>">
								</span>
								<span style="width: 293px;float:left;">
									<label style="display: block;" for="lat">longitude</label>
									<input id="lngbox" size='38' class="lng" name="lng" type="text" value="<?php echo $longitude; ?>">
								</span>
							</td>
						</tr>
						<tr id="bdAdminMap" <?php if ( !isset($_REQUEST['err']) && !isset($_REQUEST['id']) ){echo 'class="bdhidden"';} ?>>
							<td></td>
							<td>
								<div id="bdMap" style="height: 300px; width: 579px"></div>
								<?php $api_options = get_option('bd_maps_api_options'); ?>
								<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo $api_options['map_api_key']; ?>"></script>
								<script type="text/javascript">

									initialize();
									var map;
									function initialize() {
										<?php 
											if ($latitude == '') {
												$latitude = '40.713956';
												$longitude = '-74.006653';
											}
											echo 'var lat = '.$latitude.';';
											echo 'var lng = '.$longitude.';';
										?>
										var myLatlng = new google.maps.LatLng(lat,lng);

										var myOptions = {
											zoom: 8,
											center: myLatlng,
											mapTypeId: google.maps.MapTypeId.ROADMAP
										}
										map = new google.maps.Map(document.getElementById("bdMap"), myOptions); 

										var marker = new google.maps.Marker({
											draggable: true,
											position: myLatlng, 
											map: map,
											title: "Your location"
										});


										google.maps.event.addListener(marker, 'dragend', function (event) {
										    document.getElementById("latbox").value = this.getPosition().lat();
										    document.getElementById("lngbox").value = this.getPosition().lng();
										});

									}
								</script>
							</td>
						</tr>
						<tr id="bdState" <?php if ( !isset($_REQUEST['err']) && !isset($_REQUEST['id']) ){echo 'class="bdhidden"';} ?>>
							<th scope="row">State</th>
							<td>
								<input size='80' class="bd-state" name="address" value="<?php echo $state; ?>" >
							</td>
						</tr>
						<tr id="bdCountry" <?php if ( !isset($_REQUEST['err']) && !isset($_REQUEST['id']) ){echo 'class="bdhidden"';} ?>>
							<th scope="row">Country</th>
							<td>
								<input size='80' class="bd-country" name="address" value="<?php echo $country; ?>" >
							</td>
						</tr>

						<?php endif ?>

						<?php if (isset($display_options['email'])): ?>
						<tr>
							<th scope="row">Email</th>
							<td>
								<input size='80' class="email" name="email" type="text" value="<?php echo $email; ?>">
							</td>
						</tr>
						<?php endif ?>
						<?php if (isset($display_options['contact_info'])): ?>
						<tr>
							<th scope="row">Contact Information</th>
							<td>
								<textarea class="contact_info" name="contact_info" cols="78" rows="4"><?php echo $contact_info; ?></textarea>
							</td>
						</tr>
						<?php endif ?>
						<?php if (isset($display_options['services_offered'])): ?>
						<tr>
							<th scope="row">Services Offered</th>
							<td>
								<?php
								global $wpdb;
								$current_so = [];
								$current_id =  isset($_REQUEST['id']) ? $_REQUEST['id'] : '';
								if (isset($_REQUEST['id'])) {
									$services_offered = $wpdb->get_results("SELECT * FROM $db_link WHERE location_id = $current_id AND blog_id = $blog_id", ARRAY_A);
									foreach ($services_offered as $c_so) {
										array_push($current_so, $c_so['so_id']);
									}
								}
								
								$all_services_offered = $wpdb->get_results("SELECT * FROM $db_so", ARRAY_A);
								foreach ($all_services_offered as $so) {
									$so_name = $so['services_offered']; 
									$checked = '';
									if (in_array($so['id'], $current_so)) {
										$checked = 'checked';
									}
									?>
									<div class="so-input-wrap"><input <?php echo $checked; ?> class="bd-so-input-check" size="80" name="services_offered" type="checkbox" value="<?php echo $so['id']; ?>"><label for="services_offered"><?php echo $so_name; ?></label></div>
								<?php } ?>
							</td>
						</tr>
						<?php endif ?>
						<?php if (isset($display_options['store_hours'])): ?>
						<tr>
							<th scope="row">Store Hours</th>
							<td>
								<textarea class="store_hours" name="store_hours" cols="78" rows="4"><?php echo $store_hours; ?></textarea>
							</td>
						</tr>
						<?php endif ?>
						<?php if (isset($display_options['website'])): ?>
						<tr>
							<th scope="row">Company Website</th>
							<td>
								<input size='80' class="dealer_website" name="dealer_website" type="text" value="<?php echo $dealer_website; ?>">
							</td>
						</tr>
						<?php endif ?>
						<?php if (isset($display_options['image'])): ?>
						<tr>
							<th scope="row">Image</th>
							<td>
								<img style="display: none" id="locationImage" width="150" src="<?php echo $image; ?>" alt="<?php echo $store_name; ?>">
								<input style="display: none" id="bd_image_location" type="text" name="image_location" value="<?php echo $image; ?>" size="50" />
 								<input style="display:block;" class="bd-media-upload button" type="button" value="Upload Image" />
							</td>
						</tr>
						<?php endif ?>
						<?php if (isset($display_options['additional_information'])): ?>
						<tr>
							<th scope="row">Additional Information</th>
							<td>
								<textarea class="additional_information" name="additional_information" cols="78" rows="4"><?php echo $additional_information; ?></textarea>
							</td>
						</tr>
						<?php endif ?>
					</table>
					<?php 
					$action = '';
					if (isset($_REQUEST['id'])) {
						$action = 'edit';
					} ?>
					<p data-id="<?php echo $locationID; ?>" class="bd-data-submit <?php echo $action; ?>"><input value="<?php echo $buttonLabel; ?> Location" class="button button-primary" type="submit"></p>
					<?php if (isset($_REQUEST['id'])) { ?>
					<a style="float:right;" href="../wp-admin/admin.php?page=bd-locations&loc=true" class="back-to-add">Back to add new location</a>
					<?php } ?>
				</form>

				
				<?php
				global $wpdb;
				if (empty($_REQUEST['lpage'])) {
					$lpage = 0;
				}else{
					$lpage = $_REQUEST['lpage'];
				}
				$lpageOffset = $lpage * 50; //page number times amount per page
				$results = $wpdb->get_results("SELECT * FROM $db_table LIMIT 50 OFFSET $lpageOffset");
				?>
				<h2>Locations</h2>
				<div class="bd-filter-select">
					<h5>Filter By:</h5>
					<!-- <select name="so" class="bd-filter-locations" id="bdSoSelector">
						<option value="">Service</option> -->
						<?php
						// $all_services_offered = $wpdb->get_results("SELECT * FROM $db_so", ARRAY_A);
						// foreach ($all_services_offered as $so) {
						// 	$so_name = $so['services_offered'];
						// 	$val = strtolower($so_name); 
						// 	$val = preg_replace("/[\s]+/","-",$val);
						// 	echo '<option value="'.$val.'">'.$so_name.'</option>';
						// }
						?>	
					<!-- </select> -->

					<?php $states = $wpdb->get_results( "SELECT DISTINCT state FROM $db_table ORDER BY state" , ARRAY_A);?>
					<p class="filter-err" style="color:#f94d4d;display: none;">Please select at least one option</p>
					<select name="state" class="bd-filter-locations" id="bdStateSelector">
						<option value="">State</option>
						<?php
						foreach ($states as $state) {
							// $val = strtolower($state['state']);
							// $val = preg_replace("/[\s]+/","-",$val);
							echo '<option value="'.$state['state'].'">'.$state['state'].'</option>';
						}
						?>	
					</select>

					<?php $countries = $wpdb->get_results( "SELECT DISTINCT country FROM $db_table ORDER BY country" , ARRAY_A);?>
					<select name="state" class="bd-filter-locations" id="bdCountrySelector">
						<option value="">Country</option>
						<?php
						foreach ($countries as $country) {
							// $val = strtolower($country['country']);
							// $val = preg_replace("/[\s]+/","-",$val);
							echo '<option value="'.$country['country'].'">'.$country['country'].'</option>';
						}
						?>	
					</select>
					<button class="bd-filter-locations-btn">Go</button>
					<button class="bd-clear-search"><a href="../wp-admin/admin.php?page=bd-locations&loc=true">Clear</a></button>
				</div>
				<div class="locations">
					<?php
					$i=0;
					foreach ($results as $location) {
						$bd_id = $location->id;
						$bd_store_name = $location->store_name;
						$bd_store_number = $location->store_number;
						$bd_contact_info = $location->contact_info;
						$bd_email = $location->email;
						$bd_additional_info = $location->additional_information;
						$bd_address = $location->address;
						$bd_services_offered = isset($location->services_offered) ? $location->services_offered : '';
						$bd_store_hours = $location->store_hours;
						$bd_dealer_website = $location->dealer_website;
						$bd_dealer_image = $location->image;
						$lat = $location->lat;
						$lng = $location->lng;
						$state = strtolower($location->state);
						$country = strtolower($location->country);
						$country = preg_replace("/[\s]+/","-",$country);
						$badChars = array('<pre>','</pre>', ',');
						$url_address = preg_replace("/[\s]+/","+",$bd_address);
						$url_address = str_replace($badChars, '', $url_address);
						$display_options = get_option('bd_maps_display_fields');
						$bd_so = $wpdb->get_results("SELECT * FROM $db_link WHERE location_id = $bd_id AND blog_id = $blog_id", ARRAY_A);
						$services = '';
						$sClass = '';
						foreach ($bd_so as $so) {
							$so_id = $so['so_id'];
							$blog_id = $so['blog_id'];
							$service = $wpdb->get_row( "SELECT * FROM $db_so WHERE id = $so_id" , ARRAY_A);
							if(count($bd_so) > 0){
								$services .= '<p>'.$service['services_offered'].'</p>';
								$s = strtolower($service['services_offered']);
								$sClass .= preg_replace("/[\s]+/","-",$s).' ';
							}
						}	
						?>
						<div id="<?php echo $bd_id; ?>" class="dealer-location <?php echo $state.' '.$country.' '.$sClass; ?>">
							<?php if (isset($display_options['store_name'])){ ?><h3 class="admin-store"><?php echo $bd_store_name; ?><?php if (isset($display_options['store_number'])): ?>(<?php echo $bd_store_number; ?>)<?php endif ?></h3><?php } ?>
							<table class="admin-dealer-info" width="100%">
								<tr>
									<?php if (isset($display_options['image'])): ?>
									<td style="width: 20%"><img style="width: 70%;height: auto;" src="<?php echo $bd_dealer_image; ?>" /></td>
									<?php endif ?>
									<td style="width: 50%">
										<?php if (isset($display_options['contact_info'])): ?>
										<strong>Contact Info:</strong><pre><?php echo $bd_contact_info; ?></pre><br>
										<?php endif ?>
										<?php if (isset($display_options['email'])): ?>
										<strong>Email:</strong><br><?php echo $bd_email; ?><br><br>
										<?php endif ?>
										<?php if (isset($display_options['address'])): ?>
										<strong>Address:</strong><pre><?php echo $bd_address; ?></pre><br>
										<?php endif ?>
										<?php if (isset($display_options['store_hours'])): ?>
										<strong>Store Hours:</strong><br><?php echo $bd_store_hours; ?><br><br>
										<?php endif ?>
										<?php if (isset($display_options['website'])): ?>
										<strong>Website:</strong><br><a href="<?php echo $bd_dealer_website; ?>" target="_blank"><?php echo $bd_dealer_website; ?></a><br><br>
										<?php endif ?>
										<?php if (isset($display_options['store_hours'])): ?>
										<strong>Store Hours:</strong><br><?php echo $bd_store_hours; ?><br><br>
										<?php endif ?>
										<?php if (isset($display_options['additional_information'])): ?>
										<strong>Additional Information:</strong><pre><?php echo $bd_additional_info; ?><pre><br>
										<?php endif ?>
									</td>
									<?php if (isset($display_options['services_offered'])): ?>
									<td>
										<p><strong>Services Offered:</strong></p>
										<?php
											echo $services;
										?>
									</td>
									<?php endif ?>
								</tr>
							</table>
							<div class="bd-actions-container">
								<a class="bd-expand" href="#"><span class="dashicons dashicons-arrow-up-alt2"></span></a>
								<a href="/wp-admin/admin.php?page=bd-locations&loc=true&action=edit&id=<?php echo $bd_id; ?>"><span class="dashicons dashicons-edit"></span></a>
								<a class="bd-delete" href="<?php echo plugin_dir_url(__FILE__).'bdDeleteData.php?id='.$bd_id ?>"><span class="dashicons dashicons-no"></span></a>
							</div>
							<a target="_blank" href="https://www.google.com/maps/place/<?php echo $url_address; ?>/@<?php echo $lat; ?>,<?php echo $lng; ?>">Is this the correct location?</a>
						</div>
						<?php $i++;
					} 
					?>
					<div class="f12-pagination-wrap">
						<?php
						if (isset($_REQUEST['lpage'])) {
							$lpage = $_REQUEST['lpage'];
							if (empty($lpage)) {
								$lpage = 1;
							}
						}
						
							
						// dont show if on the first page
						if ($lpage > 1): ?>
						<a href="/wp-admin/admin.php?page=bd-locations&loc=true&lpage=<?php echo $lpage - 1; ?>" class="f12-prev"><< Previous</a>
						<?php endif;
						$locationsCount = $wpdb->get_var("SELECT COUNT(*) FROM $db_table");
						$locationsPageNum = ceil($locationsCount / 50);
						for ($i=1; $i <= $locationsPageNum; $i++) { 
							if ($i == $lpage) { ?>
								<a class="f12-nolink" href="javascript:;"><?php echo $i; ?></a>
						<?php 
							}else{ ?>
								<a href="/wp-admin/admin.php?page=bd-locations&loc=true&lpage=<?php echo $i; ?>"><?php echo $i; ?></a>
						<?php 
							}
						} 
						if ($lpage != $locationsPageNum) { ?>
						<a href="/wp-admin/admin.php?page=bd-locations&loc=true&lpage=<?php echo $lpage + 1; ?>" <?php echo $lpage + 1; ?> class="f12-next">Next >></a>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php }



// ==================================
// create add options
// add the admin settings and such
// ==================================
add_action('admin_init', 'bd_maps_settings');
function bd_maps_settings(){
	register_setting( 'bd_maps_plugin_options', 'bd_maps_api_options');
	add_settings_section('bd_maps_plugin_main', '', '', 'bd_maps_plugin');
	add_settings_field(  
	    'map_api_key',                      
	    'API Key',               
	    'map_api_key_field',   
	    'bd_maps_plugin',                     
	    'bd_maps_plugin_main'
	);
	add_settings_field(  
	    'users_location',                      
	    'Get Visitors Location',               
	    'bd_user_location',   
	    'bd_maps_plugin',                     
	    'bd_maps_plugin_main'
	);
}
function map_api_key_field() {
	$options = get_option('bd_maps_api_options');
	echo "<input id='map_api_key' name='bd_maps_api_options[map_api_key]' size='80' type='text' value='{$options['map_api_key']}' /><p>Once you create create an API key you must also enable the Google Maps JavaScript API and the Google Maps Geocoding API.</p><a href='https://console.developers.google.com/flows/enableapi?apiid=maps_backend,geocoding_backend,directions_backend,distance_matrix_backend,elevation_backend,places_backend&keyType=CLIENT_SIDE&reusekey=true&pli=1' target='_blank' style='display: block; margin-top: 10px;' >Get and API Key</a> ";
} 
function bd_user_location() {
	$maps_options = get_option('bd_maps_api_options');
	$checked = '';
	if( isset($maps_options['users_location']) ){
		$checked = 'checked';
	}
	echo "<input id='users_location' name='bd_maps_api_options[users_location]' type='checkbox' value='yes' ".$checked." />";
} 


add_action('admin_init', 'bd_map_settings_1');
function bd_map_settings_1(){
	register_setting( 'bd_map_settings_1', 'maps_settings_option');
	add_settings_section('bd_map', '', '', 'bd_maps');
	add_settings_field(  
	    'pin_color',                      
	    'Map Pin Color',               
	    'bd_map_pin_color',   
	    'bd_maps',                     
	    'bd_map'
	);
	add_settings_field(  
	    'map_styles',                      
	    'Map Styles <br> (Javascript style array)',               
	    'bd_map_styles',   
	    'bd_maps',                     
	    'bd_map'
	);
	add_settings_field(  
	    'search_placeholder',                      
	    'Search Box Placeholder Text',               
	    'bd_search_placeholder',   
	    'bd_maps',                     
	    'bd_map'
	);
	add_settings_field(  
	    'search_btn',                      
	    'Search Button Text',               
	    'bd_search_btn',   
	    'bd_maps',                     
	    'bd_map'
	);
}
function bd_map_pin_color(){
	$display_options = get_option('maps_settings_option');
	echo '<input id="pointerColor" value="'.$display_options['pin_color'].'">';
	echo '<input id="pinColor" name="maps_settings_option[pin_color]" type="hidden" value="'.$display_options['pin_color'].'">';
}
function bd_map_styles(){
	$display_options = get_option('maps_settings_option');
	echo '<textarea cols="80" rows="10" id="mapStyle" name="maps_settings_option[map_styles]">'.$display_options['map_styles'].'</textarea>';
	echo '<a style="display: block;" href="https://snazzymaps.com" target="_blank">Styled maps here</a>';
}
function bd_search_placeholder(){
	$display_options = get_option('maps_settings_option');
	echo '<input size="80" name="maps_settings_option[search_placeholder]" value="'.$display_options['search_placeholder'].'">';
}
function bd_search_btn(){
	$display_options = get_option('maps_settings_option');
	echo '<input size="80" name="maps_settings_option[search_btn]" value="'.$display_options['search_btn'].'">';
}

add_action('admin_init', 'bd_maps_display_settings');
function bd_maps_display_settings(){
	register_setting( 'bd_maps_display_settings', 'bd_maps_display_fields');
	add_settings_section('bd_maps_display', '', '', 'bd_maps_d');
	add_settings_field(  
	    'display_fields',                      
	    'Fields to Display',               
	    'bd_maps_display_function',   
	    'bd_maps_d',                     
	    'bd_maps_display'
	);
}
function bd_maps_display_function(){
	$display_options = get_option('bd_maps_display_fields');
	if( isset($maps_options['users_location']) ){
		$checked = 'checked';
	}
	echo '<input type="checkbox" name="bd_maps_display_fields[store_name]" value="yes"'. ( (isset($display_options['store_name'])) ? 'checked' : '') .'>Store Name<br>
	<input type="checkbox" name="bd_maps_display_fields[store_number]" value="yes" '. ( (isset($display_options['store_number'])) ? 'checked' : '') .'>Store Number<br>
	<input type="checkbox" name="bd_maps_display_fields[address]" value="yes" '. ( (isset($display_options['address'])) ? 'checked' : '') .'>Address<br>
	<input type="checkbox" name="bd_maps_display_fields[email]" value="yes" '. ( (isset($display_options['email'])) ? 'checked' : '') .'>Email<br>
	<input type="checkbox" name="bd_maps_display_fields[contact_info]" value="yes" '. ( (isset($display_options['contact_info'])) ? 'checked' : '') .'>Contact Info<br>
	<input type="checkbox" name="bd_maps_display_fields[services_offered]" value="yes" '. ( (isset($display_options['services_offered'])) ? 'checked' : '') .'>Service Offered<br>
	<input type="checkbox" name="bd_maps_display_fields[store_hours]" value="yes" '. ( (isset($display_options['store_hours'])) ? 'checked' : '') .'>Store Hours<br>
	<input type="checkbox" name="bd_maps_display_fields[website]" value="yes" '. ( (isset($display_options['website'])) ? 'checked' : '') .'>Website<br>
	<input type="checkbox" name="bd_maps_display_fields[image]" value="yes" '. ( (isset($display_options['image'])) ? 'checked' : '') .'>Image<br>
	<input type="checkbox" name="bd_maps_display_fields[additional_information]" value="yes" '. ( (isset($display_options['additional_information'])) ? 'checked' : '') .'>Additional Information<br>';	
}




// ==================================
// create SQL tables
// ==================================
function bd_google_maps_table_fn() {
	global $wpdb;

	$table_name = $wpdb->prefix . 'bd_google_maps';
	
	$charset_collate = $wpdb->get_charset_collate();

	$sql = "CREATE TABLE $table_name (
		id int NOT NULL AUTO_INCREMENT,
		store_name mediumtext NOT NULL,
		store_number mediumtext NOT NULL,
		contact_info longtext NOT NULL,
		address longtext NOT NULL,
		state mediumtext NOT NULL,
		country mediumtext NOT NULL,
		county mediumtext NOT NULL,
		email mediumtext NOT NULL,
		store_hours longtext NOT NULL,
		dealer_website tinytext NOT NULL,
		image longtext NOT NULL,
		additional_information longtext NOT NULL,
		lat float NOT NULL, 
		lng float NOT NULL,
		UNIQUE KEY id (id)
	) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );
}
register_activation_hook( __FILE__, 'bd_google_maps_table_fn' );

//create table for linking services offered to a location
function bd_google_maps_link() {
	global $wpdb;

	$table_name = $wpdb->prefix . 'bd_google_maps_link';
	
	$charset_collate = $wpdb->get_charset_collate();

	$sql = "CREATE TABLE $table_name (
		id int NOT NULL AUTO_INCREMENT,
		location_id int,
		so_id int,
		blog_id int,
		UNIQUE KEY id (id)
	) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );
}
register_activation_hook( __FILE__, 'bd_google_maps_link' );

//create Services Offered table
function bd_google_maps_so() {
	global $wpdb;

	$table_name = $wpdb->prefix . 'bd_google_maps_so';
	
	$charset_collate = $wpdb->get_charset_collate();

	$sql = "CREATE TABLE $table_name (
		id int NOT NULL AUTO_INCREMENT,
		services_offered mediumtext NOT NULL,
		UNIQUE KEY id (id)
	) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );
}
register_activation_hook( __FILE__, 'bd_google_maps_so' );

// creates new table when a blog is created (multisite)
function on_create_blog( $blog_id, $user_id, $domain, $path, $site_id, $meta ) {
    if ( is_plugin_active_for_network( 'bd-google-maps/bd-goolge-maps-geolocator.php' ) ) {
        switch_to_blog( $blog_id );
        bd_google_maps_so();
        bd_google_maps_link();
        bd_google_maps_table_fn();
        restore_current_blog();
    }
}
add_action( 'wpmu_new_blog', 'on_create_blog', 10, 6 );

// delete table when a blog is deleted (multisite)
function on_delete_blog( $tables ) {
    global $wpdb;
    $tables = array(
    	$wpdb->prefix . 'bd_google_maps'
    );
    return $tables;
}
add_filter( 'wpmu_drop_tables', 'on_delete_blog' );



// ==================================
// Print Map Page
// ==================================
function print_map($atts){
	global $db_link;
	global $db_table;
	global $db_so;
	$map_options = get_option('maps_settings_option');
	$api_options = get_option('bd_maps_api_options');
	$a = shortcode_atts( array(
        'scrollwheel' => 'true',
        'zoom' => '5'
    ), $atts );

    if ( isset($atts['scrollwheel']) ) {
    	if ($atts['scrollwheel'] === 'false') {
    		$scrollwheel = 'false';
    	}else{
    		$scrollwheel = 'true';
    	}
    }else{
    	$scrollwheel = 'true';
    }
	?>
	<div class="bd_map-container">
   	 <div class="bd_map" data-scroll="<?php echo $scrollwheel; ?>" data-zoom="<?php echo $a['zoom'] ?>"><div id="bd_show_map" style="width: 100%; height: 460px"></div></div>
   	</div>
	<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=<?php echo $api_options['map_api_key']; ?>"></script>
	<script type="text/javascript">
		var markersArr = [];
		window.onload = initialize();
		var marker;
	    <?php global $wpdb; ?>
	    function initialize() {
	    	<?php
	    	$locations = $wpdb->get_results("SELECT * FROM $db_table");
	    	$start_lat = $locations[0]->lat;
	    	$start_lng = $locations[0]->lng;
	    	?>
	        var firstLatlng = new google.maps.LatLng(<?php echo $start_lat ?>, <?php echo $start_lng ?>);              
	        var firstOptions = {
	            zoom: <?php echo $a['zoom']; ?>,
	            center: firstLatlng,
	            scrollwheel: <?php echo $scrollwheel; ?>,
	            mapTypeId: google.maps.MapTypeId.ROADMAP,
	            <?php if ($map_options['map_styles']) { ?>
	            styles: <?php echo $map_options['map_styles']; ?>
	        	<?php } ?>
	        };
	        var map = new google.maps.Map(document.getElementById("bd_show_map"), firstOptions);
	       
	    	
	        var markers = [
	        <?php 
		    $i = 0;
	    	foreach ($locations as $location) {
	    		$location_id = $location->id;
	    		$display_options = get_option('bd_maps_display_fields');
	    		$lat = $location->lat;
	    		$lng = $location->lng;
	    		$name = $location->store_name;
	    		// $name = addslashes($name);

	    		$email = $location->email;
	    		$email = addslashes($email);

	    		$image = $location->image;

	    		$contact_info = $location->contact_info;
	    		$contact_info = preg_replace("/[\n\r]+/","<br>",$contact_info);

	    		$additional_information = $location->additional_information;
	    		$additional_information = preg_replace("/[\n\r]+/","<br>",$additional_information);

	    		$address_raw = $location->address;
				$address_raw = preg_replace("/[\n\r]+/","&#13;",$address_raw);

				$address = $location->address;
				$address = preg_replace("/[\n\r]+/","<br>",$address);



				$all_services_offered = $wpdb->get_results("SELECT * FROM $db_link WHERE location_id = $location_id", ARRAY_A);
				$serv = '';
				foreach ($all_services_offered as $so) {
					$so_id = $so['so_id'];
					$service = $wpdb->get_row( "SELECT * FROM $db_so WHERE id = $so_id" , ARRAY_A);
					$so_name = $service['services_offered'];
					$serv .= '<span>'.$so_name.'</span>';
				}

				$store_hours = $location->store_hours;
				$store_hours = preg_replace("/[\n\r]+/","<br>",$store_hours);

				$dealer_website = $location->dealer_website;

				$store_id = $location->store_number;

				$post_meta = $wpdb->prefix.'postmeta';

	            $bd_address = $location->address;
	            $badChars = array('<pre>','</pre>', ',');

				$url_address = preg_replace("/[\s]+/","+",$bd_address);
				$url_address = str_replace($badChars, '', $url_address);
				if ( isset($map_options['pin_color']) ){
	    			$pin_color = str_replace('#','',$map_options['pin_color']);
	    		}else{
	    			$pin_color = 'F76C60';
	    		}
	   			$pin = '/wp-content/themes/challengerBreadware/assets/images/useful/pin.png';
	    		?>
		    		{
				        "title": "<?php echo $name; ?>",
				        "lat": "<?php echo $lat; ?>",	
				        "lng": "<?php echo $lng; ?>",
				        "image": "<?php echo $image; ?>",
				        "address": "<?php echo $address; ?>",
				        "email": "<?php echo $email; ?>",
				        "contact_info":"<?php echo $contact_info; ?>",
				        "store_hours":"<?php echo $store_hours; ?>",
				        "serv":"<?php echo $serv; ?>",
				        "additional_information":"<?php echo $additional_information; ?>",
				        "directions": "<a data-address='<?php echo $address_raw; ?>' data-name='<?php echo $name; ?>' class='info-change' href='#change'>Notify Us of Information Change</a>",
				        "dealer_website":"<?php echo $dealer_website ?>",
				        "pin_image": "<?php echo $pin; ?>",
				        "info_box": "<div class='bd-map-marker-info'><?php if (isset($display_options['store_name'])): ?><h3><?php echo $name; ?></h3><?php endif ?><?php if (isset($display_options['image'])): ?><div class='bd-left-col'><img src='<?php echo $image; ?>' /></div><?php endif ?><div class='bd-right-col'><?php if (isset($display_options['address'])): $address = addslashes($address);?><p><?php echo $address; ?></p><p><a class='bd-dealer-email' href='mailto:<?php echo $email; ?>><?php echo $email; ?></a></p><?php endif ?><?php if (isset($display_options['contact_info'])): ?><p><?php echo $contact_info; ?></p><?php endif ?><?php if (isset($display_options['store_hours'])): ?><p><?php echo $store_hours; ?></p><?php endif ?><?php if (isset($display_options['services_offered'])): ?><p><?php echo $serv; ?></p><?php endif ?><?php if (isset($display_options['additional_information'])): ?><p><?php echo $additional_information; ?></p><?php endif ?></div><div class='info-links'><a data-address='<?php echo $address_raw; ?>' data-name='<?php echo $name; ?>' class='info-change' href='#change'>Notify Us of Information Change</a><?php if (isset($display_options['website'])): ?><a class='bd-dealer-website' target='_blank' href='<?php echo $dealer_website ?>'>Website</a><?php endif ?></div></div>"
				    },
		    	<?php 
		    		$i++;
		    	} ?>
	    	]

	    	//Create and open InfoWindow.
	        var infowindow = new google.maps.InfoWindow();
	        var marker, i;
	 
	        for (var i = 0; i < markers.length; i++) {
	            var data = markers[i];
	            var myLatlng = new google.maps.LatLng(data.lat, data.lng);
	            marker = new google.maps.Marker({
	                position: myLatlng,
	                map: map,
	                title: data.title,
	                icon: data.pin_image,
	                draggable:false,
	                animation: google.maps.Animation.DROP,
	            });

	            google.maps.event.addListener(marker, 'click', (function(marker, i) {
	                return function() {
	                    infowindow.setContent(markers[i].info_box);
	                    infowindow.open(map, marker);
	                    map.setCenter(marker.getPosition());
	                    map.setZoom(12);
	                    var $innerListItem = jQuery('.location#location-'+i);
	                    var $parentDiv = jQuery('.locations-wrap');
	                    $innerListItem.addClass('active');
	                    $parentDiv.scrollTop($parentDiv.scrollTop() + $innerListItem.position().top - $parentDiv.height()/2 + $innerListItem.height()/2);
	                }
	            })(marker, i));

	            markersArr.push(marker);
	        }
	        
	    	google.maps.event.addDomListener(map, 'idle', function(){
			    var loading = document.getElementById('loading');
				loading.style.visibility = 'hidden';
			});
				
			
	    }
	    function openInfoBox(id){
		    google.maps.event.trigger(markersArr[id], 'click');
		}
    </script>
	
<?php } 
add_shortcode( 'bd_print_map', 'print_map' );



// ==================================
// show locations function
// ==================================
function show_locations($atts){
	global $wpdb;
	global $db_link;
	global $db_table;
	global $db_so;
	global $blog_id;
	$display_options = get_option('bd_maps_display_fields');
	$a = shortcode_atts( array(
        'json' => 'false',
        'user_location' => 'false',
        'amount' => '5'
    ), $atts );

    if ( isset($atts['json']) ) {
    	if ($atts['json'] === 'true') {
    		$json = 'true';
    	}else{
    		$json = 'false';
    	}
    }else{
    	$json = 'false';
    }

    if ( isset($atts['user_location']) ) {
    	if ($atts['user_location'] === 'true') {
    		$user_location = 'true';
    	}else{
    		$user_location = 'false';
    	}
    }else{
    	$user_location = 'false';
    }

    if ( isset($atts['amount']) ) {
    	$amount = $atts['amount'];
    }else{
    	$amount = $a['amount'];
    }
    if ($user_location  === 'false') {
		$locations = $wpdb->get_results("SELECT * FROM $db_table ORDER BY id DESC", ARRAY_A);
		if($json === 'true'){
			$locations =  json_encode($locations);
			return $locations;
		}else{
			?>
			<div class="bd-locations-wrap">
				<?php 
					if ( count($locations) <= $amount) {
						$num = count($locations);
					}else{
						$num = $amount;
					}
					for ($i=0; $i < $num; $i++) { 
						$store_name = $locations[$i]['store_name'];
						$location_id = $locations[$i]['id'];
						$contact_info = $locations[$i]['contact_info'];
						$address = isset($locations[$i]['address']) ? $locations[$i]['address'] : '';
						$store_hours = $locations[$i]['store_hours'];
						$dealer_website = $locations[$i]['dealer_website'];
						$email = $locations[$i]['email'];
						$additional_information = $locations[$i]['additional_information'];
						$image = $locations[$i]['image'];
						$distance = isset($locations[$i]['distance']) ? $locations[$i]['distance'] : '';
						$lat = $locations[$i]['lat'];
						$lng = $locations[$i]['lng'];
						$badChars = array('<pre>','</pre>', ',');
			            $url_address = preg_replace("/[\s]+/","+",$address);
						$url_address = str_replace($badChars, '', $address);
						$bd_address = $locations[$i]['address'];
			            $badChars = array('<pre>','</pre>', ',');
						$url_address = preg_replace("/[\s]+/","+",$bd_address);
						$url_address = str_replace($badChars, '', $url_address);
						?>
						<div class="bd-locations-container wo-distance">
							<div>
								<?php if ($image && isset($display_options['image'])): ?>
									<img src="<?php echo $image ?>" />
								<?php endif ?>
							</div>
							<div>
							<?php if ($store_name != '' && isset($display_options['store_name']) ): ?>
								<h3><?php echo $store_name; ?></h3>
							<?php endif ?>
							<?php if ($address && isset($display_options['address'])): ?>
								<h4>Address</h4>
								<p><?php echo nl2br($address); ?></p>
							<?php endif ?>
							<?php if ($contact_info != '' && isset($display_options['contact_info'])): ?>
								<h4>Contact</h4>
								<p><?php echo nl2br($contact_info); ?></p>
							<?php endif ?>
							<?php if ($store_hours && isset($display_options['store_hours'])): ?>
								<h4>Store Hours</h4>
								<p><?php echo nl2br($store_hours); ?></p>
							<?php endif ?>
							<?php if ($email && isset($display_options['email'])): ?>
								<h4>Email</h4>
								<a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a><br></p>
							<?php endif ?>
							<?php if ($additional_information && isset($display_options['additional_information'])): ?>
								<h4>Additional Information</h4>
								<p><?php echo nl2br($additional_information); ?></p>
							<?php endif ?>
							</div>
							<div>
							<?php if (isset($display_options['services_offered'])) :
								$bd_so = $wpdb->get_results("SELECT * FROM $db_link WHERE location_id = $location_id AND blog_id = $blog_id", ARRAY_A);
								$n = 0;								
								foreach ($bd_so as $so) {
									$so_id = $so['so_id'];
									$service = $wpdb->get_row( "SELECT * FROM $db_so WHERE id = $so_id" , ARRAY_A);
									if ($n == 0 && count($bd_so) > 0){ ?>
											<h4>Services Offered</h4>
									<?php };
									if(count($bd_so) > 0){
										echo '<p>'.$service['services_offered'].'</p>';
									}
									if ($n == count($bd_so) - 1){ ?>
									<?php };
									$n++;
								}
								$n = 0;	
							endif;?>
							<div class="bd_links">
								<a target="blank" href="https://www.google.com/maps/place/<?php echo $url_address; ?>/@<?php echo $lat; ?>,<?php echo $lng; ?>">Directions</a>
								<?php if (isset($display_options['website'])): ?>
								<a target="blank" href="<?php echo $dealer_website; ?>">Website</a>
								<?php endif; ?>
							</div>
							</div>
						</div>
				<?php } ?>
			</div>
		<?php }
	}else{
    	$bd_lat =  $_COOKIE['bd_lat'];
    	$bd_lng =  $_COOKIE['bd_lng'];
		$sql = "SELECT *, ( 
		3959 * acos( cos( radians($bd_lat) ) * 
		cos( radians( lat ) ) * 
		cos( radians( lng ) - radians($bd_lng) ) + sin( radians($bd_lat) ) * 
		sin( radians( lat ) ) ) ) 
		AS distance FROM $db_table HAVING distance < 1000 ORDER BY distance";
		$locations = $wpdb->get_results($sql, ARRAY_A);
		if ( count($locations) === 0) {
			$locations = $wpdb->get_results("SELECT * FROM $db_table ORDER BY id DESC", ARRAY_A);
		}
		if($a['json'] === 'true'){
			$locations =  json_encode($locations);
			return $locations;
		}else{ ?>
			<div class="bd-locations-wrap">
				<?php 
					if ( count($locations) <= $amount) {
						$num = count($locations);
					}else{
						$num = $amount;
					}
					for ($i=0; $i < $num; $i++) {
						$store_name = $locations[$i]['store_name'];
						$location_id = $locations[$i][id];
						$contact_info = $locations[$i]['contact_info'];
						$address = $locations[$i]['address'];
						$services_offered = $locations[$i]['services_offered'];
						$store_hours = $locations[$i]['store_hours'];
						$dealer_website = $locations[$i]['dealer_website'];
						$email = $locations[$i]['email'];
						$additional_information = $locations[$i]['additional_information'];
						$image = $locations[$i]['image'];
						$distance = $locations[$i]['distance'];
						$lat = $locations[$i]['lat'];
						$lng = $locations[$i]['lng'];
			            $url_address = preg_replace("/[\s]+/","+",$address);
						$url_address = str_replace($badChars, '', $address);
						$bd_address = $locations[$i]['address'];
			            $badChars = array('<pre>','</pre>', ',');
						$url_address = preg_replace("/[\s]+/","+",$bd_address);
						$url_address = str_replace($badChars, '', $url_address);
						?>
						<div class="bd-locations-container wo-distance">
							<div>
								<?php if ($image && isset($display_options['image'])): ?>
									<img src="<?php echo $image ?>" />
								<?php endif ?>
							</div>
							<div>
							<?php if ($store_name != '' && isset($display_options['store_name']) ): ?>
								<h3><?php echo $store_name; ?> <span>(<?php echo round($distance, PHP_ROUND_HALF_UP).' mi'?>)</span></h3>
							<?php endif ?>
							<?php if ($address && isset($display_options['address'])): ?>
								<h4>Address</h4>
								<p><?php echo nl2br($address); ?></p>
							<?php endif ?>
							<?php if ($contact_info != '' && isset($display_options['contact_info'])): ?>
								<h4>Contact</h4>
								<p><?php echo nl2br($contact_info); ?></p>
							<?php endif ?>
							<?php if ($store_hours && isset($display_options['store_hours'])): ?>
								<h4>Store Hours</h4>
								<p><?php echo nl2br($store_hours); ?></p>
							<?php endif ?>
							<?php if ($email && isset($display_options['email'])): ?>
								<h4>Email</h4>
								<a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a><br></p>
							<?php endif ?>
							<?php if ($additional_information && isset($display_options['additional_information'])): ?>
								<h4>Additional Information</h4>
								<p><?php echo nl2br($additional_information); ?></p>
							<?php endif ?>
							</div>
							<div>
							<?php if (isset($display_options['services_offered'])) :
								$bd_so = $wpdb->get_results("SELECT * FROM $db_link WHERE location_id = $location_id AND blog_id = $blog_id", ARRAY_A);
								$n = 0;								
								foreach ($bd_so as $so) {
									$so_id = $so['so_id'];
									$service = $wpdb->get_row( "SELECT * FROM $db_so WHERE id = $so_id" , ARRAY_A);
									if ($n == 0 && count($bd_so) > 0){ ?>
											<h4>Services Offered</h4>
									<?php };
									if(count($bd_so) > 0){
										echo '<p>'.$service['services_offered'].'</p>';
									}
									if ($n == count($bd_so) - 1){ ?>
									<?php };
									$n++;
								}
								$n = 0;	
							endif;?>
							<div class="bd_links">
								<a target="blank" href="https://www.google.com/maps/place/<?php echo $url_address; ?>/@<?php echo $lat; ?>,<?php echo $lng; ?>">Directions</a>
								<?php if (isset($display_options['website'])): ?>
								<a target="blank" href="<?php echo $dealer_website; ?>">Website</a>
								<?php endif; ?>
							</div>
							</div>
						</div>
							
				<?php } ?>
			</div>
		<?php }
	}	
}
add_shortcode( 'bd_locations', 'show_locations' );




// ==================================
// shortcode for search
// ==================================
function display_search($atts){
	global $db_link;
	global $db_table;
	global $db_so;
	global $wpdb;

	$a = shortcode_atts( array(
        'json' => 'false',
        'amount' => '5',
        'services_offered' => 'false'
    ), $atts );


	if ( isset($atts['json']) ) {
    	if ($atts['json'] === 'true') {
    		$json = 'true';
    	}else{
    		$json = 'false';
    	}
    }else{
    	$json = 'false';
    }

	if ( isset($atts['amount']) ) {
    	$amount = $atts['amount'];
    }else{
    	$amount = $a['amount'];
    }


	if ( isset($atts['services_offered']) ) {
    	if ($atts['services_offered'] === 'true') {
    		$class = 'so';
    	}else{
    		$class = 'no-so';
    	}
    }else{
    	$class = 'no-so';
    }


    ?>

	<form id="bd-zipcode-search-form" class="<?php echo $class; ?>" data-json="<?php echo $json; ?>" action="">
		<div class="bd-zipcode-search-container" data-amount="<?php echo $amount; ?>">
			<?php
				$maps_options = get_option('maps_settings_option');
				if( !empty($maps_options['search_placeholder']) ){
					$ph = $maps_options['search_placeholder'];
				}else{
					$ph = 'Search by City or Zipcode';
				}
				if( !empty($maps_options['search_btn']) ){
					$btn = $maps_options['search_btn'];
				}else{
					$btn = 'Search';
				}
			?>
			<input placeholder="<?php echo $ph; ?>" class="bd-zipcode-search" type="text">
		</div>
		<?php if ($class == 'so') { ?>
		<div class="bd-servicesoffered-container" >
			<select name="bd-services-offered" id="bd-services-offered">
				<option value="">Services Offered (All)</option>
			<?php 

			$services_offered = $wpdb->get_results( "SELECT * FROM $db_so" , ARRAY_A);
			foreach ($services_offered as $so) {?>
				<option value="<?php echo $so['id'] ?>"><?php echo $so['services_offered']; ?></option>
			<?php } ?>
			</select> 
		</div>
		<?php } ?>
		<div class="submit">
			<input class="bd-zicode-search-submit" value="<?php echo $btn; ?>" type="submit">
		</div>
	</form>

<?php    
}
add_shortcode( 'bd_search', 'display_search' );


// ==================================
// create ajax action and function for zipcode search
// ==================================
add_action( 'wp_ajax_search_locations', 'bd_get_search_locations' );
add_action( 'wp_ajax_nopriv_search_locations', 'bd_get_search_locations' );
function bd_get_search_locations(){
	global $db_link;
	global $db_table;
	global $db_so;
	global $wpdb;
	$blog_id = get_current_blog_id();
	$search_zip = $_REQUEST['zip'];
	$search_so = $_REQUEST['so'];
	$search_amt = $_REQUEST['amt'];
	$scroll = $_REQUEST['scroll'];
	$zoom = $_REQUEST['zoom'];
	$json = $_REQUEST['json'];
	$data = [];
	$newMap = '';
	$newData = '';
	//get lat and lng from zip
	$map_options = get_option('maps_settings_option');
	$api_options = get_option('bd_maps_api_options');
	$geolocation_url = "https://maps.googleapis.com/maps/api/geocode/json?address=".$search_zip."&key=".$api_options['map_api_key'];
	$url_address = preg_replace("/[\s]+/","+",$geolocation_url);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_URL, $url_address);
	$result = curl_exec($ch);
	curl_close($ch);
	$result = json_decode($result, true);
	$lat = $result['results']['0']['geometry']['location']['lat'];
	$lng = $result['results']['0']['geometry']['location']['lng'];
	if($search_so == '' && $search_zip != ''){
		$sql = "SELECT *, ( 
		3959 * acos( cos( radians($lat) ) * 
		cos( radians( lat ) ) * 
		cos( radians( lng ) - radians($lng) ) + sin( radians($lat) ) * 
		sin( radians( lat ) ) ) ) 
		AS distance FROM $db_table HAVING distance < 1000 ORDER BY distance LIMIT 0,$search_amt";
		$locations = $wpdb->get_results($sql, ARRAY_A);
		$start_lng = $locations[0]['lng'];
		$start_lat = $locations[0]['lat'];
	}elseif($search_so != 'Services Offered' && $search_zip != ''){
		$sql = "SELECT l.*, ( 
		3959 * acos( cos( radians($lat) ) * 
		cos( radians( lat ) ) * 
		cos( radians( lng ) - radians($lng) ) + sin( radians($lat) ) * 
		sin( radians( lat ) ) ) ) 
		AS distance FROM $db_table l INNER JOIN $db_link so ON l.id = so.location_id WHERE so.so_id = '$search_so' HAVING distance < 1000 ORDER BY distance LIMIT 0,$search_amt";
		$locations = $wpdb->get_results($sql, ARRAY_A);
		$start_lng = $locations[0]['lng'];
		$start_lat = $locations[0]['lat'];
	}elseif($search_zip == ''){
		echo 'Zip Error';
	}

	if ( count($locations) === 0) {
		'No Results';
	}else{ 
		$mapData = array();
		$mapData = '';
		$newMap .= '<script type="text/javascript">';
		$newMap .= 'var markersArr = [];';
		$newMap .= 'var firstLatlng = new google.maps.LatLng('.$start_lat.', '.$start_lng.');';      
	    $newMap .= 'var firstOptions = {';
	    $newMap .= 'zoom: '.$zoom.',';
	    $newMap .= 'center: firstLatlng,';
	    $newMap .= 'scrollwheel:false,';
	    $newMap .= 'mapTypeId: google.maps.MapTypeId.ROADMAP,';
	    $newMap .= 'styles: '.$map_options['map_styles'];
	    $newMap .= '};';
	    $newMap .= 'var map = new google.maps.Map(document.getElementById("bd_show_map"), firstOptions);';
		$newMap .= 'var markers = [';
		    $i = 0;
	    	foreach ($locations as $location) {
	    		$location_id = $location['id'];
	    		$display_options = get_option('bd_maps_display_fields');
	    		$lat = $location['lat'];
	    		$lng = $location['lng'];
	    		$name = $location['store_name'];

	    		$name_edited = str_replace("'", '&apos;', $name);
	    		// $name = addslashes($name);

	    		$email = $location['email'];
	    		$email = addslashes($email);

	    		$image = $location['image'];

	    		$contact_info = $location['contact_info'];
	    		$contact_info = preg_replace("/[\n\r]+/","<br>",$contact_info);

	    		$additional_information = $location['additional_information'];
	    		$additional_information = preg_replace("/[\n\r]+/","<br>",$additional_information);

				$address = $location['address'];
				$address = preg_replace("/[\n\r]+/","<br>",$address);

				$address_raw = $location['address'];
				$address_raw = preg_replace("/[\n\r]+/","&#13;",$address_raw);



				$all_services_offered = $wpdb->get_results("SELECT * FROM $db_link WHERE location_id = $location_id", ARRAY_A);
				$serv = '';
				foreach ($all_services_offered as $so) {
					$so_id = $so['so_id'];
					$service = $wpdb->get_row( "SELECT * FROM $db_so WHERE id = $so_id" , ARRAY_A);
					$so_name = $service['services_offered'];
					$serv .= '<span>'.$so_name.'</span>';
				}

				$store_hours = $location->store_hours;
				$store_hours = preg_replace("/[\n\r]+/","<br>",$store_hours);

				$dealer_website = $location->dealer_website;

				$store_id = $location->store_number;

				$post_meta = $wpdb->prefix.'postmeta';

	            $bd_address = $location->address;
	            $badChars = array('<pre>','</pre>', ',');

				$url_address = preg_replace("/[\s]+/","+",$bd_address);
				$url_address = str_replace($badChars, '', $url_address);
	    		if ( isset($map_options['pin_color']) ){
	    			$pin_color = str_replace('#','',$map_options['pin_color']);
	    		}else{
	    			$pin_color = 'F76C60';
	    		}
	   			$pin = 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|' . $pin_color;
	    		$mapData .= '<div id="location-'.$i.'" class="location" onclick="openInfoBox('.$i.');"><div class="col-xs-2 no-p"><div class="service-indicator serv-'.$so_id.'"><span>'.($i + 1).'</span></div></div><div class="col-xs-10 no-p"><h4 class="loc-name">'.$name.'</h4><p></p><pre>'.$address.'</pre><p></p></div><div class="clearfix"></div></div>';
		$newMap .= '{';
		$newMap .= '"title": "'.$name .'",';
		$newMap .= '"lat": "'.$lat .'",';
		$newMap .= '"lng": "'.$lng .'",';
		$newMap .= '"image": "'.$image .'",';
		$newMap .= '"address": "'.$address .'",';
		$newMap .= '"email": "'.$email .'",';
		$newMap .= '"contact_info":"'.$contact_info .'",';
		$newMap .= '"store_hours":"'.$store_hours .'",';
		$newMap .= '"serv":"'.$serv .'",';
		$newMap .= '"additional_information":"'.$additional_information .'",';
		$newMap .= '"dealer_website":"'.$dealer_website.'",';
		$newMap .= '"pin_image": "'.$pin .'",';
		$newMap .= '"info_box": "<div class=\'bd-map-marker-info\'>';
	       		if ( isset($display_options['store_name']) ):
	            	$newMap .= '<h3>'. $name .'</h3>';
	            endif;
	            if (isset($display_options['image'])):
	            	$newMap .= '<div class=\'bd-left-col\'>';
	            	$newMap .= '<img src=\''. $image .'\' />';
	            	$newMap .= '</div>';
	            endif;
	            	$newMap .= '<div class=\'bd-right-col\'>';
	            if (isset($display_options['address'])):
	            	$newMap .= '<p>'. $address .'</p>';
	            	$newMap .= '<p><a class=\'bd-dealer-email\' href=\'mailto:'. $email .'\'>'. $email .'</a></p>';
	            endif;
	            if (isset($display_options['contact_info'])):
	            	$newMap .= '<p>'. $contact_info .'</p>';
	            endif;
	            if (isset($display_options['store_hours'])):
	            	$newMap .= '<p>'. $store_hours .'</p>';
	            endif;
	            if (isset($display_options['services_offered'])):
	            	$newMap .= '<p>'. $serv .'</p>';
	            endif;
	            if (isset($display_options['additional_information'])):
	            	$newMap .= '<p>'. $additional_information .'</p>';
	            endif;
	            $newMap .= '</div>';
	            $newMap .= '<div class=\'info-links\'>';
	            $newMap .= '<a data-address=\''.$address_raw.'\' data-name=\''.$name_edited.'\' class=\'info-change\' href=\'#change\'>Notify Us of Information Change</a>';
	            if (isset($display_options['website'])):
	            	$newMap .= '<a class=\'bd-dealer-website\' target=\'_blank\' href=\''. $dealer_website .'\'>Website</a>';
	            endif;
	            $newMap .= '</div></div>"';
		$newMap .= '},';
		    		$i++;
		    	} //end locations loop
	    $newMap .= '];';

	    //Create InfoWindow
	    $newMap .= 'var infowindow = new google.maps.InfoWindow();';
	    $newMap .= 'var marker, i;';
	    $newMap .= 'for (var i = 0; i < markers.length; i++) {';
	    $newMap .= 'var data = markers[i];';
	    $newMap .= 'var myLatlng = new google.maps.LatLng(data.lat, data.lng);';
	    $newMap .= 'marker = new google.maps.Marker({';
	    $newMap .= 'position: myLatlng,';
	    $newMap .= 'map: map,';
	    $newMap .= 'title: data.title,';
	    $newMap .= 'icon: data.pin_image,';
	    $newMap .= 'draggable:false,';
	    $newMap .= 'animation: google.maps.Animation.DROP,';
	    $newMap .= '});';
	    $newMap .= 'google.maps.event.addListener(marker, "click", (function(marker, i) {';
	    $newMap .= 'return function() {';
	    $newMap .= 'infowindow.setContent(markers[i].info_box);';
	    $newMap .= 'infowindow.open(map, marker);';
	    $newMap .= 'map.setCenter(marker.getPosition());';
	    $newMap .= 'map.setZoom(12);';
	    $newMap .= 'var $innerListItem = jQuery(".location#location-"+i);';
	    $newMap .= 'var $parentDiv = jQuery(".locations-wrap");';
	    $newMap .= '$innerListItem.addClass("active");';
	    $newMap .= '$parentDiv.scrollTop($parentDiv.scrollTop() + $innerListItem.position().top - $parentDiv.height()/2 + $innerListItem.height()/2);';
	    $newMap .= '}';
	    $newMap .= '})(marker, i));';
	    $newMap .= 'markersArr.push(marker);';
	    $newMap .= '}';
	    $newMap .= 'google.maps.event.addDomListener(map, "idle", function(){';
		$newMap .= 'var loading = document.getElementById("loading");';
		$newMap .= 'loading.style.visibility = "hidden";';
		$newMap .= '});';
	    $newMap .= 'function openInfoBox(id){	';
		$newMap .= 'google.maps.event.trigger(markersArr[id], "click");';
		$newMap .= '}';
		$newMap .= '</script>';
	    	$data[0] = $newMap;
			$mapData = array();
			$index = 0;
			foreach ($locations as $location) {
				$display_options = get_option('bd_maps_display_fields');
				$map_options = get_option('maps_settings_option');
				$location_id = $location['id'];
				$lat = $location['lat'];
				$lng = $location['lng'];
				$name = $location['store_name'];
				$name = $name;
				$email = $location['email;'];
				$email = addslashes($email);
				$image = $location['image'];

				$contact_info = $location['contact_info'];
				$contact_info = preg_replace("/[\n\r]+/","<br>",$contact_info);

				$additional_information = $location['additional_information'];
				$additional_information = preg_replace("/[\n\r]+/","<br>",$additional_information);

				$address = $location['address'];
				$address = preg_replace("/[\n\r]+/","<br>",$address);

				$all_services_offered = $wpdb->get_results("SELECT * FROM $db_link WHERE location_id = $location_id", ARRAY_A);
				$serv = '';
				foreach ($all_services_offered as $so) {
					$so_id = $so['so_id'];
					$service = $wpdb->get_row( "SELECT * FROM $db_so WHERE id = $so_id" , ARRAY_A);
					$so_name = $service['services_offered'];
					$serv .= '<span>'.$so_name.'</span>';
				}

				$store_hours = $location['store_hours'];
				$store_hours = preg_replace("/[\n\r]+/","<br>",$store_hours);

				$dealer_website = $location['dealer_website'];

				$store_id = $location['store_number'];

				$post_meta = $wpdb->prefix.'postmeta';

				$bd_address = $location['address'];
				$badChars = array('<pre>','</pre>', ',');

				$url_address = preg_replace("/[\s]+/","+",$bd_address);
				$url_address = str_replace($badChars, '', $url_address);
			
				$m = '<div id="location-'.$index.'" class="location" onclick="openInfoBox('.$index.');"><div class="col-xs-2 no-p"><div class="service-indicator serv-'.$so_id.'"><span>'.($index + 1).'</span></div></div><div class="col-xs-10 no-p"><h4 class="loc-name">'.$name.'</h4><p></p><pre>'.$address.'</pre><p></p></div><div class="clearfix"></div></div>';
				array_push($mapData,$m);
				$index++;
			}

			// create html
			$data[1] = $mapData;
			echo json_encode($data);
	}
	die();
}




