<?php 

require_once('../../../wp-load.php');
global $wpdb;
$db_table = $wpdb->prefix.'bd_google_maps';
$db_link = $wpdb->prefix.'bd_google_maps_link';
$blog_id = get_current_blog_id();

// $so = isset($_REQUEST['so']) ? $_REQUEST['so'] : '';
$state = isset($_REQUEST['state']) ? $_REQUEST['state'] : '';
$country = isset($_REQUEST['country']) ? $_REQUEST['country'] : '';
$query = "SELECT * FROM ". $db_table." WHERE ";

$index = 0;
if ( $state !== '') {
	$query .= "state = ".'"'.$state.'"';
	$index++;
}
if ( $country !== '' ) {
	if ($index > 0) {
		$query .= ' AND ';
	}
	$query .= 'country = ' .'"'.$country.'"';
	$index++;
}

// if ( !empty($so) ) {
// 	$query .= 
// }
$results = $wpdb->get_results($query);
// echo json_encode($results);
$i=0;
foreach ($results as $location) {
	$bd_id = $location->id;
	$bd_store_name = $location->store_name;
	$bd_store_number = $location->store_number;
	$bd_contact_info = $location->contact_info;
	$bd_email = $location->email;
	$bd_additional_info = $location->additional_information;
	$bd_address = $location->address;
	$bd_store_hours = $location->store_hours;
	$bd_dealer_website = $location->dealer_website;
	$bd_dealer_image = $location->image;
	$lat = $location->lat;
	$lng = $location->lng;
	$state = strtolower($location->state);
	$country = strtolower($location->country);
	$country = preg_replace("/[\s]+/","-",$country);
	$badChars = array('<pre>','</pre>', ',');
	$url_address = preg_replace("/[\s]+/","+",$bd_address);
	$url_address = str_replace($badChars, '', $url_address);
	$display_options = get_option('bd_maps_display_fields');
	$bd_so = $wpdb->get_results("SELECT * FROM $db_link WHERE location_id = $bd_id AND blog_id = $blog_id", ARRAY_A);
	$services = '';
	$sClass = '';
	foreach ($bd_so as $so) {
		$so_id = $so['so_id'];
		$blog_id = $so['blog_id'];
		$service = $wpdb->get_row( "SELECT * FROM $db_so WHERE id = $so_id" , ARRAY_A);
		if(count($bd_so) > 0){
			$services .= '<p>'.$service['services_offered'].'</p>';
			$s = strtolower($service['services_offered']);
			$sClass .= preg_replace("/[\s]+/","-",$s).' ';
		}
	}	
	?>
	<div id="<?php echo $bd_id; ?>" class="dealer-location <?php echo $state.' '.$country.' '.$sClass; ?>">
		<?php if (isset($display_options['store_name'])){ ?><h3 class="admin-store"><?php echo $bd_store_name; ?><?php if (isset($display_options['store_number'])): ?>(<?php echo $bd_store_number; ?>)<?php endif ?></h3><?php } ?>
		<table class="admin-dealer-info" width="100%">
			<tr>
				<?php if (isset($display_options['image'])): ?>
				<td style="width: 20%"><img style="width: 70%;height: auto;" src="<?php echo $bd_dealer_image; ?>" /></td>
				<?php endif ?>
				<td style="width: 50%">
					<?php if (isset($display_options['contact_info'])): ?>
					<strong>Contact Info:</strong><pre><?php echo $bd_contact_info; ?></pre><br>
					<?php endif ?>
					<?php if (isset($display_options['email'])): ?>
					<strong>Email:</strong><br><?php echo $bd_email; ?><br><br>
					<?php endif ?>
					<?php if (isset($display_options['address'])): ?>
					<strong>Address:</strong><pre><?php echo $bd_address; ?></pre><br>
					<?php endif ?>
					<?php if (isset($display_options['store_hours'])): ?>
					<strong>Store Hours:</strong><br><?php echo $bd_store_hours; ?><br><br>
					<?php endif ?>
					<?php if (isset($display_options['website'])): ?>
					<strong>Website:</strong><br><a href="<?php echo $bd_dealer_website; ?>" target="_blank"><?php echo $bd_dealer_website; ?></a><br><br>
					<?php endif ?>
					<?php if (isset($display_options['store_hours'])): ?>
					<strong>Store Hours:</strong><br><?php echo $bd_store_hours; ?><br><br>
					<?php endif ?>
					<?php if (isset($display_options['additional_information'])): ?>
					<strong>Additional Information:</strong><pre><?php echo $bd_additional_info; ?><pre><br>
					<?php endif ?>
				</td>
				<?php if (isset($display_options['services_offered'])): ?>
				<td>
					<p><strong>Services Offered:</strong></p>
					<?php
						echo $services;
					?>
				</td>
				<?php endif ?>
			</tr>
		</table>
		<div class="bd-actions-container">
			<a class="bd-expand" href="#"><span class="dashicons dashicons-arrow-up-alt2"></span></a>
			<a href="/wp-admin/admin.php?page=bd-locations&loc=true&action=edit&id=<?php echo $bd_id; ?>"><span class="dashicons dashicons-edit"></span></a>
			<a class="bd-delete" href="<?php echo plugin_dir_url(__FILE__).'bdDeleteData.php?id='.$bd_id ?>"><span class="dashicons dashicons-no"></span></a>
		</div>
		<a target="_blank" href="https://www.google.com/maps/place/<?php echo $url_address; ?>/@<?php echo $lat; ?>,<?php echo $lng; ?>">Is this the correct location?</a>
	</div>
	<?php $i++;
} 

