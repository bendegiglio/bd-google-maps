<?php 
require_once('../../../wp-load.php');
global $wpdb;
$db_table = $wpdb->prefix.'bd_google_maps';
$db_link = $wpdb->prefix.'bd_google_maps_link';
$blog_id = get_current_blog_id();
$id = $_REQUEST['id'];


if (isset($_POST['store_name']) && $_POST['store_name'] != '') {
    $store_name = stripslashes(strip_tags($_POST['store_name']));
}else{
     $store_name = '';
}

if (isset($_POST['store_number']) && $_POST['store_number'] != '') {
    $store_number = stripslashes(strip_tags($_POST['store_number']));
}else{
     $store_number = '';
}

if (isset($_POST['address']) && $_POST['address'] != '') {
    $address = stripslashes(strip_tags($_POST['address']));
    $address = preg_replace("/[\n\r]+/","\n",$address);
}else{
     $address = '';
}

if (isset($_POST['lat']) && $_POST['lat'] !== '') {
    $lat = stripslashes(strip_tags($_POST['lat']));
    $lat = preg_replace("/[\n\r]+/","\n",$lat);
}else{
     $lat = '';
}

if (isset($_POST['lng']) && $_POST['lng'] !== '') {
    $lng = stripslashes(strip_tags($_POST['lng']));
    $lng = preg_replace("/[\n\r]+/","\n",$lng);
}else{
     $lng = '';
}

if (isset($_POST['contact_info']) && $_POST['contact_info'] != '') {
    $contact_info = stripslashes(strip_tags($_POST['contact_info']));
    $contact_info = preg_replace("/[\n\r]+/","\n",$contact_info);
}else{
     $contact_info = '';
}

if (isset($_POST['dealer_image']) && $_POST['dealer_image'] != '') {
    $dealer_image = $_POST['dealer_image'];
}else{
     $dealer_image = '';
}

if (isset($_POST['email']) && $_POST['email'] != '') {
    $email = $_POST['email'];
}else{
    $email = '';
}

if (isset($_POST['store_hours']) && $_POST['store_hours'] != '') {
    $store_hours = stripslashes(strip_tags($_POST['store_hours']));
    $store_hours = preg_replace("/[\n\r]+/","\n",$store_hours);
}else{
     $store_hours = '';
}

if (isset($_POST['dealer_website']) && $_POST['dealer_website'] != '') {
    $dealer_website = $_POST['dealer_website'];
}else{
     $dealer_website = '';
}

if (isset($_POST['additional_information']) && $_POST['additional_information'] != '') {
    $additional_information = $_POST['additional_information'];
}else{
     $additional_information = '';
}


$maps_options = get_option('plugin_options'); 

$geolocation_url = "https://maps.googleapis.com/maps/api/geocode/json?address=".$address."&key=".$maps_options['map_api_key'];
$url_address = preg_replace("/[\s]+/","+",$geolocation_url);

$maps_options = get_option('plugin_options'); 
$ch = curl_init();
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_URL, $url_address);
$result = curl_exec($ch);
curl_close($ch);
$result = json_decode($result, true);
$gen_state = $result['results']['0']['address_components']['4']['long_name'];
$gen_country = $result['results']['0']['address_components']['5']['long_name'];

if ($lat == '') {
   $lat = $result['results']['0']['geometry']['location']['lat'];
   $lng = $result['results']['0']['geometry']['location']['lng'];
}

if ($_POST['state'] != '') {
    $state = $_POST['state'];
}else if($gen_state != ''){
    $state = $gen_state;
}else{
     $state = '';
}

if ($_POST['country'] != '') {
    $country = $_POST['country'];
}else if($gen_country != ''){
    $country = $gen_country;
}else{
     $country = '';
}

$county = $result['results']['0']['address_components']['3']['long_name'];
if ($county == '') {
    $county = '';
}
$location_data = array(
    'store_name'  =>  $store_name,
    'store_number'  =>  $store_number,
    'address'  =>  $address,
    'state'  =>  $state,
    'country'  =>  $country,
    'county'  =>  $county,
    'email' => $email,
    "image" => $dealer_image,
    "additional_information" => $additional_information,
    'contact_info'  =>  $contact_info,
    'store_hours'  =>  $store_hours,
    'dealer_website'  =>  $dealer_website,
    'lat' => $lat,
    'lng' => $lng
);

$wpdb->update( 
	$db_table, 
	$location_data, 
	array( 'ID' => $id )
);


$wpdb->delete( $db_link, array( 'location_id' => $id, 'blog_id' => $blog_id) );

$wpdb->show_errors();


$services_offered = json_decode(stripslashes($_POST['services_offered']));


if($wpdb->last_error !== ''){
    echo 'error reg';
}else{
    $services_offered = json_decode(stripslashes($_POST['services_offered']));

    foreach ($services_offered as $so) {
        $bd_so = array(
            'location_id'  =>  $id,
            'so_id'  =>  $so,
            'blog_id' => $blog_id

        );
        $insert_data = $wpdb->insert($db_link,$bd_so);
    }


    if($wpdb->last_error !== ''){
        echo 'error serv';
    }else{
        echo 'success';
    }
}
